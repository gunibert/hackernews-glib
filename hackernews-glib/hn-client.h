/* hn-client.h
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once

#include <glib-object.h>
#include "hn-item.h"
#include "hn-api.h"
#include "hn-node.h"
#include <gio/gio.h>

G_BEGIN_DECLS

#define HN_TYPE_CLIENT (hn_client_get_type())

G_DECLARE_FINAL_TYPE (HnClient, hn_client, HN, CLIENT, GObject)

HnClient  *hn_client_new                      (gchar                *baseurl);
gchar     *hn_client_get_base_url             (HnClient             *self);
HnItem    *hn_client_get_item                 (HnClient             *self,
                                               guint                 id);
GPtrArray *hn_client_get_stories              (HnClient             *self,
                                               gint                  limit,
                                               gchar                *endpoint_id);
void       hn_client_get_stories_async        (HnClient             *self,
                                               gint                  limit,
                                               gchar                *endpoint_id,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
GPtrArray *hn_client_get_stories_finish       (HnClient             *self,
                                               GAsyncResult         *result,
                                               GError              **error);
GPtrArray *hn_client_get_comments             (HnClient             *self,
                                               HnItem               *parent,
                                               GError              **error);
void       hn_client_get_comments_async       (HnClient             *self,
                                               HnItem               *parent,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
GPtrArray *hn_client_get_comments_finish      (HnClient             *self,
                                               GAsyncResult         *result,
                                               GError              **error);
void       hn_client_get_comments_full_async  (HnClient             *self,
                                               HnItem               *root,
                                               GCancellable         *cancellable,
                                               GAsyncReadyCallback   callback,
                                               gpointer              user_data);
HnNode    *hn_client_get_comments_full_finish (HnClient             *self,
                                               GAsyncResult         *result,
                                               GError              **error);
GPtrArray *hn_client_get_available_endpoints  (HnClient             *self);
gchar     *hn_client_get_endpoint_name        (HnClient             *self,
                                               gchar                *endpoint_id);
G_END_DECLS
