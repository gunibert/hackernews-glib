/* hn-api-endpoint.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "hn-api-endpoint.h"

G_DEFINE_INTERFACE (HnApiEndpoint, hn_api_endpoint, G_TYPE_OBJECT)

static void
hn_api_endpoint_default_init (HnApiEndpointInterface *iface)
{
}

gchar *
hn_api_endpoint_get_name (HnApiEndpoint *self)
{
  return HN_API_ENDPOINT_GET_IFACE (self)->get_name (self);
}

gchar *
hn_api_endpoint_get_identifier (HnApiEndpoint *self)
{
  return HN_API_ENDPOINT_GET_IFACE (self)->get_identifier (self);
}

gchar *
hn_api_endpoint_get_uri (HnApiEndpoint *self)
{
  return HN_API_ENDPOINT_GET_IFACE (self)->get_uri (self);
}
