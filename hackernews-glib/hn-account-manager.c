/* hn-account-manager.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "hn-account-manager.h"

struct _HnAccountManager
{
  GObject parent_instance;
};

G_DEFINE_TYPE (HnAccountManager, hn_account_manager, G_TYPE_OBJECT)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

HnAccountManager *
hn_account_manager_new (void)
{
  return g_object_new (HN_TYPE_ACCOUNT_MANAGER, NULL);
}

static void
hn_account_manager_finalize (GObject *object)
{
  HnAccountManager *self = (HnAccountManager *)object;

  G_OBJECT_CLASS (hn_account_manager_parent_class)->finalize (object);
}

static void
hn_account_manager_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
  HnAccountManager *self = HN_ACCOUNT_MANAGER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_account_manager_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
  HnAccountManager *self = HN_ACCOUNT_MANAGER (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_account_manager_class_init (HnAccountManagerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = hn_account_manager_finalize;
  object_class->get_property = hn_account_manager_get_property;
  object_class->set_property = hn_account_manager_set_property;
}

static void
hn_account_manager_init (HnAccountManager *self)
{
}
