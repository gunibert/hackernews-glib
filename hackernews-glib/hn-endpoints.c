/* hn-endpoints.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include "hn-endpoints.h"
#include "hn-api-endpoint.h"

struct _HnTopStories
{
  GObject parent_instance;
};

struct _HnNewStories
{
  GObject parent_instance;
};

struct _HnAskStories
{
  GObject parent_instance;
};

struct _HnShowStories
{
  GObject parent_instance;
};

static void hn_top_stories_endpoint_iface (HnApiEndpointInterface *iface);
static void hn_new_stories_endpoint_iface (HnApiEndpointInterface *iface);
static void hn_ask_stories_endpoint_iface (HnApiEndpointInterface *iface);
static void hn_show_stories_endpoint_iface (HnApiEndpointInterface *iface);

G_DEFINE_TYPE_WITH_CODE (HnTopStories, hn_top_stories, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (HN_TYPE_API_ENDPOINT, hn_top_stories_endpoint_iface))
G_DEFINE_TYPE_WITH_CODE (HnNewStories, hn_new_stories, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (HN_TYPE_API_ENDPOINT, hn_new_stories_endpoint_iface))
G_DEFINE_TYPE_WITH_CODE (HnAskStories, hn_ask_stories, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (HN_TYPE_API_ENDPOINT, hn_ask_stories_endpoint_iface))
G_DEFINE_TYPE_WITH_CODE (HnShowStories, hn_show_stories, G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (HN_TYPE_API_ENDPOINT, hn_show_stories_endpoint_iface))

//////// TOP STORIES ////////
HnTopStories *
hn_top_stories_new ()
{
  return g_object_new (HN_TYPE_TOP_STORIES, NULL);
}

static void
hn_top_stories_class_init (HnTopStoriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
}

static void
hn_top_stories_init (HnTopStories *self)
{

}

static gchar *
hn_top_stories_get_name (HnApiEndpoint *endpoint)
{
  return "Top Stories";
}

static gchar *
hn_top_stories_get_identifier (HnApiEndpoint *endpoint)
{
  return "top-stories";
}

static gchar *
hn_top_stories_get_uri (HnApiEndpoint *endpoint)
{
  return "%s/topstories.json";
}

static void
hn_top_stories_endpoint_iface (HnApiEndpointInterface *iface)
{
  iface->get_name = hn_top_stories_get_name;
  iface->get_identifier = hn_top_stories_get_identifier;
  iface->get_uri = hn_top_stories_get_uri;
}
//////// TOP STORIES ////////


//////// NEW STORIES ////////
HnNewStories *
hn_new_stories_new ()
{
  return g_object_new (HN_TYPE_NEW_STORIES, NULL);
}

static void
hn_new_stories_class_init (HnNewStoriesClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
}

static void
hn_new_stories_init (HnNewStories *self)
{

}

static gchar *
hn_new_stories_get_name (HnApiEndpoint *endpoint)
{
  return "New Stories";
}

static gchar *
hn_new_stories_get_identifier (HnApiEndpoint *endpoint)
{
  return "new-stories";
}

static gchar *
hn_new_stories_get_uri (HnApiEndpoint *endpoint)
{
  return "%s/newstories.json";
}

static void
hn_new_stories_endpoint_iface (HnApiEndpointInterface *iface)
{
  iface->get_name = hn_new_stories_get_name;
  iface->get_identifier = hn_new_stories_get_identifier;
  iface->get_uri = hn_new_stories_get_uri;
}
//////// NEW STORIES ////////

//////// ASK STORIES ////////
HnAskStories *
hn_ask_stories_new ()
{
  return g_object_new (HN_TYPE_ASK_STORIES, NULL);
}

static void
hn_ask_stories_class_init (HnAskStoriesClass *klass)
{
}

static void
hn_ask_stories_init (HnAskStories *self)
{

}

static gchar *
hn_ask_stories_get_name (HnApiEndpoint *endpoint)
{
  return "Ask HN";
}

static gchar *
hn_ask_stories_get_identifier (HnApiEndpoint *endpoint)
{
  return "ask-stories";
}

static gchar *
hn_ask_stories_get_uri (HnApiEndpoint *endpoint)
{
  return "%s/askstories.json";
}

static void
hn_ask_stories_endpoint_iface (HnApiEndpointInterface *iface)
{
  iface->get_name = hn_ask_stories_get_name;
  iface->get_identifier = hn_ask_stories_get_identifier;
  iface->get_uri = hn_ask_stories_get_uri;
}
//////// ASK STORIES ////////

//////// SHOW STORIES ////////
HnShowStories *
hn_show_stories_new ()
{
  return g_object_new (HN_TYPE_SHOW_STORIES, NULL);
}

static void
hn_show_stories_class_init (HnShowStoriesClass *klass)
{
}

static void
hn_show_stories_init (HnShowStories *self)
{

}

static gchar *
hn_show_stories_get_name (HnApiEndpoint *endpoint)
{
  return "Show HN";
}

static gchar *
hn_show_stories_get_identifier (HnApiEndpoint *endpoint)
{
  return "show-stories";
}

static gchar *
hn_show_stories_get_uri (HnApiEndpoint *endpoint)
{
  return "%s/showstories.json";
}

static void
hn_show_stories_endpoint_iface (HnApiEndpointInterface *iface)
{
  iface->get_name = hn_show_stories_get_name;
  iface->get_identifier = hn_show_stories_get_identifier;
  iface->get_uri = hn_show_stories_get_uri;
}
//////// SHOW STORIES ////////
