/* hn-item.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:hn-item
 * @Short_description: the data object for all elements of HN api
 * @Title: HnItem
 *
 */

#include "hn-item.h"
#include "hn-item-enums.h"

struct _HnItem
{
  GObject parent_instance;

  gint64 id;
  gchar *title;
  gchar *text;
  gchar *url;
  gchar *author;
  GDateTime *creation_time;
  guint score;
  GArray *children;
  gint descendants;
  gboolean deleted;
  HnItemType type;

  guint pos;
};

G_DEFINE_TYPE (HnItem, hn_item, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_ID,
  PROP_TITLE,
  PROP_TEXT,
  PROP_URL,
  PROP_AUTHOR,
  PROP_CREATION_TIME,
  PROP_SCORE,
  PROP_CHILDREN,
  PROP_DESCENDANTS,
  PROP_DELETED,
  PROP_ITEM_TYPE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

HnItem *
hn_item_new (void)
{
  return g_object_new (HN_TYPE_ITEM, NULL);
}

static void
hn_item_finalize (GObject *object)
{
  HnItem *self = (HnItem *)object;

  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->text, g_free);
  g_clear_pointer (&self->url, g_free);
  g_clear_pointer (&self->author, g_free);
  g_clear_pointer (&self->creation_time, g_date_time_unref);

  g_array_free (self->children, TRUE);

  G_OBJECT_CLASS (hn_item_parent_class)->finalize (object);
}

static void
hn_item_get_property (GObject    *object,
                      guint       prop_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
  HnItem *self = HN_ITEM (object);

  switch (prop_id)
    {
    case PROP_ID:
      g_value_set_int64 (value, hn_item_get_id (self));
      break;
    case PROP_TITLE:
      g_value_set_string (value, hn_item_get_title (self));
      break;
    case PROP_TEXT:
      g_value_set_string (value, hn_item_get_text (self));
      break;
    case PROP_URL:
      g_value_set_string (value, hn_item_get_url (self));
      break;
    case PROP_AUTHOR:
      g_value_set_string (value, hn_item_get_author (self));
      break;
    case PROP_CREATION_TIME:
      break;
    case PROP_SCORE:
      g_value_set_uint (value, hn_item_get_score (self));
      break;
    case PROP_CHILDREN:
      g_value_set_pointer (value, hn_item_get_children (self));
      break;
    case PROP_DESCENDANTS:
      g_value_set_int (value, hn_item_get_descendants (self));
      break;
    case PROP_DELETED:
      g_value_set_boolean (value, hn_item_get_deleted (self));
      break;
    case PROP_ITEM_TYPE:
      g_value_set_enum (value, hn_item_get_item_type (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_item_set_property (GObject      *object,
                      guint         prop_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
  HnItem *self = HN_ITEM (object);

  switch (prop_id)
    {
    case PROP_ID:
      hn_item_set_id (self, g_value_get_int64 (value));
      break;
    case PROP_TITLE:
      hn_item_set_title (self, g_value_get_string (value));
      break;
    case PROP_TEXT:
      hn_item_set_text (self, g_value_get_string (value));
      break;
    case PROP_URL:
      hn_item_set_url (self, g_value_get_string (value));
      break;
    case PROP_AUTHOR:
      hn_item_set_author (self, g_value_get_string (value));
      break;
    case PROP_SCORE:
      hn_item_set_score (self, g_value_get_uint (value));
      break;
    case PROP_CHILDREN:
      self->children = g_value_get_pointer (value);
      break;
    case PROP_DESCENDANTS:
      hn_item_set_descendants (self, g_value_get_int (value));
      break;
    case PROP_DELETED:
      hn_item_set_deleted (self, g_value_get_boolean (value));
      break;
    case PROP_ITEM_TYPE:
      hn_item_set_item_type (self, g_value_get_enum (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
hn_item_class_init (HnItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = hn_item_finalize;
  object_class->get_property = hn_item_get_property;
  object_class->set_property = hn_item_set_property;

  properties [PROP_ID] =
    g_param_spec_int64 ("id",
                        "Id",
                        "Id",
                        G_MININT64,
                        G_MAXINT64,
                        0,
                        (G_PARAM_READWRITE |
                         G_PARAM_STATIC_STRINGS));

  properties [PROP_TITLE] =
    g_param_spec_string ("title",
                         "Title",
                         "Title",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_TEXT] =
    g_param_spec_string ("text",
                         "Text",
                         "Text",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_URL] =
    g_param_spec_string ("url",
                         "Url",
                         "Url",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_AUTHOR] =
    g_param_spec_string ("author",
                         "Author",
                         "Author",
                         "",
                         (G_PARAM_READWRITE |
                          G_PARAM_STATIC_STRINGS));

  properties [PROP_CREATION_TIME] =
    g_param_spec_pointer ("creation-time",
                          "CreationTime",
                          "CreationTime",
                          (G_PARAM_READWRITE |
                           G_PARAM_STATIC_STRINGS));

  properties [PROP_SCORE] =
    g_param_spec_int ("score",
                      "Score",
                      "Score",
                      0,
                      INT_MAX,
                      0,
                      (G_PARAM_READWRITE |
                       G_PARAM_STATIC_STRINGS));

  properties [PROP_CHILDREN] =
    g_param_spec_pointer ("children",
                          "Children",
                          "Children",
                          (G_PARAM_READWRITE |
                           G_PARAM_STATIC_STRINGS));

  properties [PROP_DESCENDANTS] =
    g_param_spec_int ("descendants",
                      "Descendants",
                      "Descendants",
                      0,
                      INT_MAX,
                      0,
                      (G_PARAM_READWRITE |
                       G_PARAM_STATIC_STRINGS));

  properties [PROP_DELETED] =
    g_param_spec_boolean ("deleted",
                          "Deleted",
                          "Deleted",
                          FALSE,
                          (G_PARAM_READWRITE |
                           G_PARAM_STATIC_STRINGS));

  properties [PROP_ITEM_TYPE] =
    g_param_spec_enum ("item-type",
                       "ItemType",
                       "ItemType",
                       HN_TYPE_ITEM_TYPE,
                       HN_ITEM_TYPE_STORY,
                       (G_PARAM_READWRITE |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
hn_item_init (HnItem *self)
{
  self->pos = 0;
  self->children = g_array_new (TRUE, TRUE, sizeof (gint64));
}

gint64
hn_item_get_id (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), 0);

  return self->id;
}

void
hn_item_set_id (HnItem *self,
                gint64  id)
{
  g_return_if_fail (HN_IS_ITEM (self));

  self->id = id;
}

gchar *
hn_item_get_title (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), NULL);

  return self->title;
}

void
hn_item_set_title (HnItem      *self,
                   const gchar *title)
{
  g_return_if_fail (HN_IS_ITEM (self));
  g_return_if_fail (title != NULL);

  g_clear_pointer (&self->title, g_free);
  self->title = g_strdup (title);
}

gchar *
hn_item_get_text (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), NULL);

  return self->text;
}

void
hn_item_set_text (HnItem      *self,
                  const gchar *text)
{
  g_return_if_fail (HN_IS_ITEM (self));
  g_return_if_fail (text != NULL);

  g_clear_pointer (&self->text, g_free);
  self->text = g_strdup (text);
}

gchar *
hn_item_get_url (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), NULL);

  return self->url;
}

void
hn_item_set_url (HnItem      *self,
                 const gchar *url)
{
  g_return_if_fail (HN_IS_ITEM (self));
  g_return_if_fail (url != NULL);

  g_clear_pointer (&self->url, g_free);
  self->url = g_strdup (url);
}

gchar *
hn_item_get_author (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), NULL);

  return self->author;
}

void
hn_item_set_author (HnItem      *self,
                    const gchar *author)
{
  g_return_if_fail (HN_IS_ITEM (self));
  g_return_if_fail (author != NULL);

  g_clear_pointer (&self->author, g_free);
  self->author = g_strdup (author);
}

GDateTime *
hn_item_get_creation_time (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), NULL);

  return self->creation_time;
}

void
hn_item_set_creation_time (HnItem *self,
                           gint64  ct)
{
  g_return_if_fail (HN_IS_ITEM (self));

  g_clear_pointer (&self->creation_time, g_date_time_unref);
  self->creation_time = g_date_time_new_from_unix_local (ct);
}

guint
hn_item_get_score (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), 0);

  return self->score;
}

void
hn_item_set_score (HnItem *self,
                   guint   score)
{
  g_return_if_fail (HN_IS_ITEM (self));

  self->score = score;
}

guint
hn_item_get_pos (HnItem *self)
{
  return self->pos;
}

void
hn_item_set_pos (HnItem *self,
                 guint   pos)
{
  self->pos = pos;
}

/**
 * hn_item_get_children:
 * @self: a #HnItem
 *
 * Returns: (element-type gint64) (transfer none): returns all children identifier for this #HnItem
 */
GArray *
hn_item_get_children (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), NULL);

  return self->children;
}

void
hn_item_add_child (HnItem *self,
                   gint64  child_id)
{
  g_return_if_fail (HN_IS_ITEM (self));

  g_array_append_val (self->children, child_id);
}

gint
hn_item_get_descendants (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), 0);

  return self->descendants;
}

void
hn_item_set_descendants (HnItem *self,
                         gint    descendants)
{
  g_return_if_fail (HN_IS_ITEM (self));

  self->descendants = descendants;
}

gboolean
hn_item_get_deleted (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), FALSE);

  return self->deleted;
}

void
hn_item_set_deleted (HnItem   *self,
                     gboolean  deleted)
{
  g_return_if_fail (HN_IS_ITEM (self));

  self->deleted = deleted;
}

HnItemType
hn_item_get_item_type (HnItem *self)
{
  g_return_val_if_fail (HN_IS_ITEM (self), HN_ITEM_TYPE_STORY);

  return self->type;
}

void
hn_item_set_item_type (HnItem     *self,
                       HnItemType  type)
{
  g_return_if_fail (HN_IS_ITEM (self));

  self->type = type;
}
