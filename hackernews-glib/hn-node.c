/* hn-node.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

/**
 * SECTION:hn-node
 * @Short_description: a boxed alternative to #GNode
 * @Title: HnNode
 *
 */

#include "hn-node.h"

G_DEFINE_BOXED_TYPE (HnNode, hn_node, hn_node_ref, hn_node_unref)

/**
 * hn_node_new:
 * @data: the data
 *
 * Creates a new #HnNode.
 *
 * Returns: (transfer full): A newly created #HnNode
 */
HnNode *
hn_node_new (gpointer data)
{
  HnNode *self;

  self = g_slice_new0 (HnNode);
  self->ref_count = 1;

  self->data = data;

  return self;
}

/**
 * hn_node_copy:
 * @self: a #HnNode
 *
 * Makes a deep copy of a #HnNode.
 *
 * Returns: (transfer full): A newly created #HnNode with the same
 *   contents as @self
 */
HnNode *
hn_node_copy (HnNode *self)
{
  HnNode *copy;

  g_return_val_if_fail (self, NULL);
  g_return_val_if_fail (self->ref_count, NULL);

  copy = hn_node_new (self->data);

  return copy;
}

static void
hn_node_free (HnNode *self)
{
  g_assert (self);
  g_assert_cmpint (self->ref_count, ==, 0);

  g_object_unref (self->data);

  g_slice_free (HnNode, self);
}

/**
 * hn_node_ref:
 * @self: A #HnNode
 *
 * Increments the reference count of @self by one.
 *
 * Returns: (transfer full): @self
 */
HnNode *
hn_node_ref (HnNode *self)
{
  g_return_val_if_fail (self, NULL);
  g_return_val_if_fail (self->ref_count, NULL);

  g_atomic_int_inc (&self->ref_count);

  return self;
}

/**
 * hn_node_unref:
 * @self: A #HnNode
 *
 * Decrements the reference count of @self by one, freeing the structure when
 * the reference count reaches zero.
 */
void
hn_node_unref (HnNode *self)
{
  g_return_if_fail (self);
  g_return_if_fail (self->ref_count);

  if (g_atomic_int_dec_and_test (&self->ref_count))
    hn_node_free (self);
}

HnNode *
hn_node_append (HnNode *parent,
                HnNode *child)
{
  g_return_val_if_fail (parent != NULL, child);
  g_return_val_if_fail (child != NULL, child);

  HnNode *sibling = NULL;

  child->parent = parent;

  if (parent->children)
    {
      sibling = parent->children;
      while (sibling->next)
        sibling = sibling->next;
      child->prev = sibling;
      sibling->next = child;
    }
  else
    {
      child->parent->children = child;
    }

  return child;
}

gboolean
hn_node_traverse_pre_order (HnNode             *node,
                            GTraverseFlags      flags,
                            HnNodeTraverseFunc  func,
                            gpointer            data)
{
  if (node->children)
    {
      HnNode *child;

      if ((flags & G_TRAVERSE_NON_LEAFS) && func (node, data))
	      return TRUE;

      child = node->children;
      while (child)
	      {
	        HnNode *current;

	        current = child;
	        child = current->next;
	        if (hn_node_traverse_pre_order (current, flags, func, data))
	          return TRUE;
	      }
    }
  else if ((flags & G_TRAVERSE_LEAFS) && func (node, data))
    return TRUE;

  return FALSE;
}

guint
hn_node_depth (HnNode *node)
{
  guint depth = 0;

  while (node)
    {
      depth++;
      node = node->parent;
    }

  return depth;
}
