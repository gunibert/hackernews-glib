import gi

# This is just to be able to run the script from the builddir of meson
gi.require_version('GIRepository', '2.0')
from gi.repository import GIRepository
GIRepository.Repository.prepend_search_path("@girdir@")
GIRepository.Repository.prepend_library_path("@girdir@")
# This is just to be able to run the script from the builddir of meson

gi.require_version('Hn', '1.0')
from gi.repository import GObject, GLib, Gio, Hn

if __name__ == "__main__":
    client = Hn.Client.new("https://hacker-news.firebaseio.com/v0/")
    client.get_stories(-1, "top-stories")
