public static void main (string[] args)
{
    Hn.Client client = new Hn.Client ("https://hacker-news.firebaseio.com/v0/");

    var stories = client.get_stories (-1, "new-stories");
    stories.foreach ( (item) => {
        print(item.get_title ());
    });
}
