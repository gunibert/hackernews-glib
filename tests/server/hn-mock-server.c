/* hn-mock-server.c
 *
 * Copyright 2019 Günther Wagner <info@gunibert.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <libsoup/soup.h>
#include <gio/gio.h>
#include <gio/gunixsocketaddress.h>

static SoupServer *server;
static GMainLoop *loop;
static GThread *t;

void
hn_mock_server_item_handler (SoupServer        *server,
                             SoupMessage       *msg,
                             const char        *path,
                             GHashTable        *query,
                             SoupClientContext *client,
                             gpointer           user_data)
{
  gchar *id = g_strrstr (path, "/");

  if (id == NULL) return;
  g_print ("handler got called: %s\n", ++id);

  const gchar *data = g_test_get_filename (G_TEST_DIST, "server", id, NULL);
  g_autofree gchar *contents;
  g_file_get_contents (data, &contents, NULL, NULL);
  soup_message_set_status (msg, 200);
  soup_message_set_response (msg, "application/json", SOUP_MEMORY_COPY, contents, strlen (contents));
}

void
hn_mock_server_story_handler (SoupServer        *server,
                             SoupMessage       *msg,
                             const char        *path,
                             GHashTable        *query,
                             SoupClientContext *client,
                             gpointer           user_data)
{
  g_print ("%s\n", path);
  gchar *contents = "[1, 2, 3, 4]";
  soup_message_set_status (msg, 200);
  soup_message_set_response (msg, "application/json", SOUP_MEMORY_COPY, contents, strlen (contents));
}

gpointer
hn_mock_server_run (gpointer user_data)
{
	GMainContext *context = g_main_context_new ();
	g_main_context_push_thread_default (context);
  loop = g_main_loop_new (context, FALSE);
  soup_server_listen_local (server, 10000, 0, NULL);

  g_main_loop_run (loop);
  return NULL;
}

void
hn_mock_server_start ()
{
  server = soup_server_new (NULL, NULL);

  soup_server_add_handler (server, "/item", hn_mock_server_item_handler, NULL, NULL);
  soup_server_add_handler (server, "/", hn_mock_server_story_handler, NULL, NULL);

  t = g_thread_new (NULL, hn_mock_server_run, server);
}

void
hn_mock_server_stop ()
{
  g_main_loop_quit (loop);
  g_main_loop_unref (loop);
  g_object_unref (server);
  server = NULL;
  g_thread_unref (t);
}
