#include <glib.h>
#include "hackernews-glib.h"
#include "server/hn-mock-server.h"

void
start_mock_server (GObject       *fixture,
                   gconstpointer  user_data)
{
  hn_mock_server_start ();
}

void
stop_mock_server (GObject       *fixture,
                  gconstpointer  user_data)
{
  hn_mock_server_stop ();
}

void
test_api_endpoints (GObject       *fixture,
                    gconstpointer  user_data)
{
  HnClient *client = hn_client_new ("http://localhost:10000");

  GPtrArray *endpoints = hn_client_get_available_endpoints (client);
  g_assert_cmpint (4, ==, endpoints->len);

  gchar *first_endpoint_id = g_ptr_array_index (endpoints, 0);
  g_assert_cmpstr ("top-stories", ==, first_endpoint_id);
  g_autofree gchar *first_endpoint_name = hn_client_get_endpoint_name (client, first_endpoint_id);
  g_assert_cmpstr ("Top Stories", ==, first_endpoint_name);

  hn_client_get_stories (client, 4, first_endpoint_id);

  g_ptr_array_free (endpoints, FALSE);
  g_object_unref (client);
}

void
test_api_get_items (GObject      *fixture,
                    gconstpointer user_data)
{
  HnClient *client = hn_client_new ("http://localhost:10000");

  HnItem *item = hn_client_get_item (client, 8863);
  g_assert_nonnull (item);

  gchar *title = hn_item_get_title (item);
  g_assert_cmpstr (title , ==, "My YC app: Dropbox - Throw away your USB drive");

  gchar *url = hn_item_get_url (item);
  g_assert_cmpstr (url, ==, "http://www.getdropbox.com/u/2/screencast.html");

  gchar *author = hn_item_get_author (item);
  g_assert_cmpstr (author, ==, "dhouston");

  GDateTime *creation_time = hn_item_get_creation_time (item);
  g_assert_cmpint (g_date_time_to_unix (creation_time), ==, 1175714200);

  g_object_unref (item);
  g_object_unref (client);
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/api/endpoints", GObject, NULL, start_mock_server, test_api_endpoints, stop_mock_server);
  g_test_add ("/api/get_items", GObject, NULL, start_mock_server, test_api_get_items, stop_mock_server);

  return g_test_run ();
}
