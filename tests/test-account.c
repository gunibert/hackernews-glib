#include <glib.h>
#include "hackernews-glib.h"
#include "server/hn-mock-server.h"
#include "hn-account-manager.h"

void
test_account_login_success (void)
{
  HnAccountManager *manager = hn_account_manager_new ();
}

gint
main (gint   argc,
      gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/account/login/success", test_account_login_success);

  return g_test_run ();
}
